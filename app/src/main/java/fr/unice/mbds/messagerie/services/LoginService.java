package fr.unice.mbds.messagerie.services;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import fr.unice.mbds.messagerie.helpers.ContactHelper;

public class LoginService {
    private static LoginService ourInstance = null;

    ContactHelper mDbHelper;


    public static LoginService getInstance(Context context) {
        if (ourInstance == null){
            ourInstance = new LoginService (context);
        }
        return ourInstance;
    }

    private LoginService(Context context) {
        mDbHelper = new ContactHelper ( context );
    }


    public final class ContactContract {
        private ContactContract() {
        }

        public class FeedLogin implements BaseColumns{

            public static final String TABLE_NAME = "ContactFragment";
            public static final String COLUMN_NAME_USERNAME= "username";
            public static final String COLUMN_NAME_FIRSTNAME = "firstName";
            public static final String COLUMN_NAME_LASTNAME= "lastName";
            public static final String COLUMN_NAME_CONFIRMPASSWORD = "confPassWord";
            public static final String COLUMN_NAME_PASSWORD = "passWord";
            public static final String COLUMN_NAME_SHORTDESCRIPTION = "shortDescription";

        }
    }

    public void addLogin(String username, String firstName, String lastName, String confPassWord, String passWord, String shortDescription )
    {
        // Gets the data repository in write mode

        SQLiteDatabase db = mDbHelper.getWritableDatabase ();

// Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(LoginService.ContactContract.FeedLogin.COLUMN_NAME_USERNAME, username);
        values.put(LoginService.ContactContract.FeedLogin.COLUMN_NAME_FIRSTNAME, firstName);
        values.put(LoginService.ContactContract.FeedLogin.COLUMN_NAME_LASTNAME, lastName);
        values.put(LoginService.ContactContract.FeedLogin.COLUMN_NAME_PASSWORD, passWord);
        values.put(LoginService.ContactContract.FeedLogin.COLUMN_NAME_CONFIRMPASSWORD, confPassWord);
        values.put(LoginService.ContactContract.FeedLogin.COLUMN_NAME_SHORTDESCRIPTION, shortDescription);



// Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(LoginService.ContactContract.FeedLogin.TABLE_NAME, null, values);
    }

}
