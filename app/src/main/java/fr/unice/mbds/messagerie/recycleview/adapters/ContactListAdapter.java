package fr.unice.mbds.messagerie.recycleview.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import fr.unice.mbds.messagerie.R;
import fr.unice.mbds.messagerie.models.ContactModel;
import fr.unice.mbds.messagerie.recycleview.viewholder.ContactListViewHolder;

public class ContactListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ContactModel> contactList;

    public ContactListAdapter(List<ContactModel> contactList) {
        this.contactList = contactList;
    }

    public void add(List<ContactModel> contactList) {
        this.contactList = contactList;
        this.notifyDataSetChanged(); // to render the list we need to notify
    }


    @Override
    public ContactListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.contact_list_recycler_view_row, parent, false);

        ContactListViewHolder contactListViewHolder = new ContactListViewHolder(itemView);

        return contactListViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ContactModel contactModel = contactList.get(position);
        ((ContactListViewHolder) holder).username.setText(contactModel.getUserName());
        ((ContactListViewHolder) holder).shortdescription.setText(contactModel.getShortdescription());
        ((ContactListViewHolder) holder).avatar.setImageResource(R.drawable.default_avatar);
    }

    @Override
    public int getItemCount() {
        return contactList.size();
    }


}