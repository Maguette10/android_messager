package fr.unice.mbds.messagerie.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import fr.unice.mbds.messagerie.R;
import fr.unice.mbds.messagerie.activities.AddContactActivity;
import fr.unice.mbds.messagerie.helpers.AuthHelper;
import fr.unice.mbds.messagerie.iCallable;
import fr.unice.mbds.messagerie.models.ContactModel;
import fr.unice.mbds.messagerie.recycleview.adapters.ContactListAdapter;
import fr.unice.mbds.messagerie.services.APIService;
import fr.unice.mbds.messagerie.services.ContactService;

public class ContactListFragment extends Fragment {

    // Activities
    private iCallable mainActivityCallable;

    // Recycle view
    private ContactListAdapter contactListAdapter;
    private RecyclerView contactListRecycleView;

    // Services
    private ContactService database;

    // Props
    private HashMap<String, ContactModel> contactList = new HashMap<String, ContactModel>();
    private Button addNewContactBtn;

    private JsonArrayRequest getMyContactListRequest;
    private ContactModel currentUser;

    public ContactListFragment() {
    }

    /**
     * on récupère un pointeur vers l'activité contenante
     *
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof iCallable) {
            mainActivityCallable = (iCallable) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement iCallable ");
        }
    }

    /**
     * on instancie les objets non graphiques
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //
        this.contactListAdapter = new ContactListAdapter(this.getContactList());

        //
        this.database = ContactService.getInstance ( getActivity () );
    }

    /**
     * on instancie la vue et les composants graphiques
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        return inflater.inflate(R.layout.contact_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.contactListRecycleView = (RecyclerView) view.findViewById(R.id.contact_list_recycler_view);
        this.addNewContactBtn = (Button) view.findViewById(R.id.add_contact_btn);

        this.contactListRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.contactListRecycleView.setItemAnimator(new DefaultItemAnimator());
        this.contactListRecycleView.setAdapter(this.contactListAdapter);

        // Listeners
        this.addNewContactBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                this.switchToAddContactActivity();
            }

            private void switchToAddContactActivity() {
                System.out.println("switch To Add Contact Activity");
                Intent contactsIntent = new Intent(getActivity(), AddContactActivity.class);
                contactsIntent.putExtra("username", "test");
                startActivity(contactsIntent);
            }
        });

        this.contactListRecycleView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {

            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent motionEvent) {
                ArrayList<ContactModel> contacts = getContactList();

                if (currentUser == null || contacts.isEmpty()) return false;

                if(motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    View child = rv.findChildViewUnder(motionEvent.getX(), motionEvent.getY());

                    int pos = rv.getChildAdapterPosition(child);

                    ContactModel clickedContactRow = contacts.get(pos);
                    String username = clickedContactRow != null ? clickedContactRow.getUserName() : null;

                    System.out.println("Post (" + pos + ")  --> " + clickedContactRow);

                    if (username != null) {
                        mainActivityCallable.transferData(username);
                    }

                }
                return false;
            }

            @Override
            public void onTouchEvent(@NonNull RecyclerView recyclerView, @NonNull MotionEvent motionEvent) {
            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean b) {
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

        this.currentUser = ContactModel.getCurrentUser();
        this.fetchMyContactList(this.currentUser);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (this.getMyContactListRequest != null) {
            this.getMyContactListRequest.cancel();
        }
    }

    private ArrayList<ContactModel> getContactList() {
        return new ArrayList<ContactModel>(this.contactList.values());
    }

    public void fetchMyContactList(final ContactModel user) {
        String url = APIService.URL + "fetchMessages";
        this.getMyContactListRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Context ctx = getActivity();

                        Toast.makeText(ctx, "User list received", Toast.LENGTH_SHORT).show();

                        addContacts(response, user);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Context ctx = getActivity();

                        Toast.makeText(ctx, "MainActivity error :/\n" + error, Toast.LENGTH_SHORT).show();
                        Log.v("MAIN_ACTIVITY", error.toString());
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                Context ctx                 = getActivity();
                String accessToken          = AuthHelper.readAcessToken(ctx);

                headers.put("Authorization" , "Bearer " + accessToken);

                return headers;
            }
        };

        APIService.getInstance(getActivity()).addToRequestQueue(this.getMyContactListRequest);
    }

    private void addContacts(JSONArray messages, ContactModel user) {
        for (int i = 0; i < messages.length(); i++) {
            JSONObject message = null;
            try {
                message = messages.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            addContactFromJSONObject(message, user);
        }

        this.contactListAdapter.add(this.getContactList());
    }

    private void addContactFromJSONObject(JSONObject msg, ContactModel user) {
        try {
            String username = msg.getString("author");

            if (username.equals(user.getUserName())) { return; }

            ContactModel contact  = new ContactModel(username);

            this.contactList.put(username, contact);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setText(String username) {
        this.currentUser = ContactModel.getCurrentUser();
        this.fetchMyContactList(this.currentUser);
    }

    private void initData() {
        contactList.put("Issoufi", new ContactModel("Issoufi", "Adam", "Issoufi", "test", "The king of MIAGE"));
        contactList.put("Maguette", new ContactModel("Maguette", "Yacine", "Maguette", "test", "The last day music"));
        contactList.put("George", new ContactModel("Issoufi", "Adam", "George", "test", "Search a true Love"));
        contactList.put("King", new ContactModel("Issoufi", "Adam", "King", "test", "Need money, please..."));
        contactList.put("Fred", new ContactModel("Issoufi", "Adam", "Fred", "test", "Strainge is the life, life is the streng"));
        contactList.put("Maxime", new ContactModel("Issoufi", "Adam", "Maxime", "test", "Master 2 act 3"));
        contactList.put("David", new ContactModel("Issoufi", "Adam", "David", "test", "Tout est sprial et tout est amour..."));
    }


}
