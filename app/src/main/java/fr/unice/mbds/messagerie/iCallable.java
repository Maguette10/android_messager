package fr.unice.mbds.messagerie;

public interface iCallable {
    void transferData(String s);
}
