package fr.unice.mbds.messagerie.services;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;

import java.util.ArrayList;
import java.util.List;

import fr.unice.mbds.messagerie.helpers.ContactHelper;
import fr.unice.mbds.messagerie.models.ContactModel;

public class ContactService {
    private static ContactService ourInstance = null;
    ContactHelper mDbHelper;

    public static ContactService getInstance(Context context) {
        if (ourInstance == null) {
            ourInstance = new ContactService(context);
        }
        return ourInstance;
    }

    private ContactService(Context context) {
        mDbHelper = new ContactHelper(context);
    }


    public final class ContactContract {
        private ContactContract() {
        }

        public class FeedContact implements BaseColumns {
            public static final String TABLE_NAME = "ContactListFragment";
            public static final String COLUMN_NAME_LASTNAME = "Lastname";
            public static final String COLUMN_NAME_FIRSTNAME = "Firstname";
            public static final String COLUMN_NAME_USERNAME = "Username";
            public static final String COLUMN_NAME_PASSWORD = "Password";
            public static final String COLUMN_NAME_SHORT_DESCRIPTION = "ShortDescription";
        }

    }

    public void addPerson(String s, String firstname, String lastname, String username, String shortdescription) {
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(ContactService.ContactContract.FeedContact.COLUMN_NAME_LASTNAME, lastname);
        values.put(ContactService.ContactContract.FeedContact.COLUMN_NAME_FIRSTNAME, firstname);
        values.put(ContactService.ContactContract.FeedContact.COLUMN_NAME_USERNAME, username);
        //values.put(ContactService.ContactContract.FeedContact.COLUMN_NAME_PASSWORD, password);
        values.put(ContactService.ContactContract.FeedContact.COLUMN_NAME_SHORT_DESCRIPTION, shortdescription);

        // Insert the new row, returning the primary key value of the new row
        long newRowId = db.insert(ContactService.ContactContract.FeedContact.TABLE_NAME, null, values);
    }


    public List<ContactModel> readPerson() {

        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        String[] projection = {
                BaseColumns._ID,
                ContactService.ContactContract.FeedContact.COLUMN_NAME_LASTNAME,
                ContactService.ContactContract.FeedContact.COLUMN_NAME_FIRSTNAME,
                ContactService.ContactContract.FeedContact.COLUMN_NAME_USERNAME,
                ContactService.ContactContract.FeedContact.COLUMN_NAME_PASSWORD,
                ContactService.ContactContract.FeedContact.COLUMN_NAME_SHORT_DESCRIPTION
        };


        String selection = "";
        String[] selectionArgs = null;

        String sortOrder =
                ContactService.ContactContract.FeedContact.COLUMN_NAME_LASTNAME + " DESC";

        Cursor cursor = db.query(
                ContactService.ContactContract.FeedContact.TABLE_NAME,   // The table to query
                null,  // The array of columns to return (pass null to get all)
                selection,      // The columns for the WHERE clause
                selectionArgs,  // The values for the WHERE clause
                null,  // don't group the rows
                null,    // don't filter by row groups
                sortOrder       // The sort order
        );

        List persons = new ArrayList<ContactModel>();
        while (cursor.moveToNext()) {
            long itemId = cursor.getLong(cursor.getColumnIndexOrThrow(ContactService.ContactContract.FeedContact._ID));
            String lastname = cursor.getString(cursor.getColumnIndex(ContactService.ContactContract.FeedContact.COLUMN_NAME_LASTNAME));
            String firstname = cursor.getString(cursor.getColumnIndex(ContactService.ContactContract.FeedContact.COLUMN_NAME_FIRSTNAME));
            String username = cursor.getString(cursor.getColumnIndex(ContactService.ContactContract.FeedContact.COLUMN_NAME_USERNAME));
            String password = cursor.getString(cursor.getColumnIndex(ContactService.ContactContract.FeedContact.COLUMN_NAME_PASSWORD));
            String shortdescription = cursor.getString(cursor.getColumnIndex(ContactService.ContactContract.FeedContact.COLUMN_NAME_SHORT_DESCRIPTION));
            persons.add(new ContactModel(firstname, lastname, username,password,shortdescription));

        }
        cursor.close();

        return persons;
    }

}
