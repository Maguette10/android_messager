package fr.unice.mbds.messagerie.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;

import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import fr.unice.mbds.messagerie.R;
import fr.unice.mbds.messagerie.services.LoginService;

public class RegisterActivity extends AppCompatActivity {
    private EditText username;
    private EditText firtName;
    private EditText lastName;
    private EditText shortDescription;
    private EditText passBox;
    private EditText confirmpassBox;
    private Button confirmBtn;
    private Button cancelBtn;

    private LoginService database;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        //
        super.onCreate(savedInstanceState);
        setContentView(	R.layout.register_activity );

        //
        this.username = findViewById(R.id.register_username_box);
        this.firtName = findViewById(R.id.register_firstname_box);
        this.lastName = findViewById(R.id.register_lastname_box);
        this.shortDescription = findViewById(R.id.register_short_description_box);
        this.passBox =  findViewById(R.id.register_pass_box);
        this.confirmpassBox = findViewById(R.id.resgister_confirmpass_box);
        this.confirmBtn = findViewById(R.id.register_confirm_btn);
        this.cancelBtn = findViewById(R.id.register_cancel_btn);
        this.database = LoginService.getInstance ( this );

        //
        this.cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchToLoginActivity();
            }
        });
        this.confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                database.addLogin (username.getText().toString (),firtName.getText ().toString (),
                        lastName.getText ().toString (), passBox.getText ().toString (),confirmpassBox.getText ().toString (),
                        shortDescription.getText ().toString ());
                String trimedUsername = username.getText().toString().trim();
                switchToLoginActivity(trimedUsername);
            }
        });
    }


    public void onConfirmBtnClick(View v){
        String username = this.username.getText().toString().trim();
        String password = this.passBox.getText().toString();
        String confirmPassword = this.confirmpassBox.getText().toString();

        if(!password.equals(confirmPassword)) { this.anErrorHasOccurred(); }
        if(username.isEmpty() || password.isEmpty() || confirmPassword.isEmpty()) { this.anErrorHasOccurred();}

        this.switchToLoginActivity(username);
    }

    private void anErrorHasOccurred() {
        this.confirmBtn.setBackgroundColor(Color.RED);
    }

    private void emptyInputEditText() {
        this.username.setText(null);
        this.firtName.setText(null);
    }


    private void switchToLoginActivity() {
        switchToLoginActivity(null);
    }

    private void switchToLoginActivity(String extraData) {
        this.emptyInputEditText();

        Intent intentLogin = new Intent(RegisterActivity.this, LoginActivity.class);
        if (extraData != null) {
            intentLogin.putExtra("RegisterActivity", extraData);
        }
        startActivity(intentLogin);
    }

}
