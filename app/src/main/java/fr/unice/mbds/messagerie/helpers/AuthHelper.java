package fr.unice.mbds.messagerie.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import fr.unice.mbds.messagerie.R;

public class AuthHelper {

    public static void writeAcessToken(Context context, String accessToken) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(context.getString(R.string.access_token), accessToken);
        editor.commit();

        Toast.makeText(context, "write access_token " + accessToken, Toast.LENGTH_SHORT).show();
    }

    public static String readAcessToken(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.preference_file_key), Context.MODE_PRIVATE);

        String defaultValue = "undefined";
        String accessToken = sharedPreferences.getString(context.getString(R.string.access_token), defaultValue);

        //Toast.makeText(context, "read access_token " + accessToken, Toast.LENGTH_SHORT).show();

        return accessToken;
    }
}
