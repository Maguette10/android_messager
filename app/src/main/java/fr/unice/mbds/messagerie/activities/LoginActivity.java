package fr.unice.mbds.messagerie.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

import fr.unice.mbds.messagerie.R;
import fr.unice.mbds.messagerie.helpers.AuthHelper;
import fr.unice.mbds.messagerie.models.ContactModel;
import fr.unice.mbds.messagerie.services.APIService;
import fr.unice.mbds.messagerie.services.LoginService;

public class LoginActivity extends AppCompatActivity {
    private EditText loginBox;
    private EditText passBox;
    private Button seConnecterBtn;
    private Button registerBtn;

    // Service
    private LoginService database;

    // Requests
    private JsonObjectRequest loginRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        this.loginBox = findViewById(R.id.login_username_box);
        this.passBox = findViewById(R.id.login_pass_box);
        this.seConnecterBtn = findViewById(R.id.login_seconnecter_btn);
        this.registerBtn = findViewById(R.id.login_register_btn);

        //
        this.database = LoginService.getInstance(this);


        // Set the title bar
        getSupportActionBar().setTitle("Log In");

        // init listeners
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentLogin = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intentLogin);
            }
        });

        //
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                handleSendText(intent); // Handle text being sent
            }
        }
    }



    public void handleSendText(Intent intent) {
        String sharedText = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (sharedText != null) {
            // Update UI to reflect text being shared
        }
    }

    public void login(View v) {
        String username = this.loginBox.getText().toString();
        String password = this.passBox.getText().toString();

        String url = APIService.URL + "login";
        JSONObject loginPayload = createLoginPayload(username, password);

        Toast.makeText(LoginActivity.this, "Please wait...", Toast.LENGTH_SHORT).show();

        this.loginRequest = new JsonObjectRequest(
                Request.Method.POST,
                url,
                loginPayload,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                        Toast.makeText(LoginActivity.this, "Connected", Toast.LENGTH_SHORT).show();

                        String accessToken = getAcessTokenFromJSONObject(response);

                        AuthHelper.writeAcessToken(LoginActivity.this, accessToken);

                        setCurrentUser(response);
                        emptyInputEditText();
                        switchToMainActivity();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(LoginActivity.this, "Login error :/\n" + error, Toast.LENGTH_SHORT).show();
                        Log.v("LOGIN", error.toString());

                        seConnecterBtn.setBackgroundColor(Color.RED);
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return super.getHeaders();
            }
        };

        APIService.getInstance(this).addToRequestQueue(loginRequest);
    }

    private JSONObject createLoginPayload(String username, String password) {
        JSONObject payload = new JSONObject();
        try {
            payload.put("username", username);
            payload.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return payload;
    }

    private String getAcessTokenFromJSONObject(JSONObject json) {
        String accessToken = null;
        try {
            accessToken = json.getString("access_token");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return accessToken;
    }

    private void setCurrentUser(JSONObject json) {
        try {
            String username = json.getString("username");
            ContactModel currentUser = new ContactModel(username);

            ContactModel.setCurrentUser(currentUser);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void emptyInputEditText() {
        loginBox.setText(null);
        passBox.setText(null);
    }

    private void switchToMainActivity() {
        Intent contactsIntent = new Intent(LoginActivity.this, MainActivity.class);
        contactsIntent.putExtra("LoginActivity", loginBox.getText().toString().trim());
        emptyInputEditText();
        startActivity(contactsIntent);
    }

    @Override
    protected void onStop () {
        super.onStop();
        if (loginRequest != null) {
            loginRequest.cancel();
        }
    }
}
