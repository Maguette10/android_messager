package fr.unice.mbds.messagerie.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.unice.mbds.messagerie.R;
import fr.unice.mbds.messagerie.helpers.AuthHelper;
import fr.unice.mbds.messagerie.iCallable;
import fr.unice.mbds.messagerie.models.ContactModel;
import fr.unice.mbds.messagerie.models.MessageModel;
import fr.unice.mbds.messagerie.recycleview.adapters.MessageListAdapter;
import fr.unice.mbds.messagerie.services.APIService;

public class MessageListFragment extends Fragment {

    // Activities
    private iCallable mainActivityCallable;

    // Recycle view
    private MessageListAdapter messageListAdapter;
    private RecyclerView messageListRecycleView;


    // Props
    private List<MessageModel> messageList = new ArrayList<>();

    //
    private EditText messageEditText;
    private ImageButton messageSendBtn;
    private JsonArrayRequest getMessageRequest;
    private ContactModel fromUser, withUser;

    /**
     * Default constructor
     */
    public MessageListFragment() { }

    /**
     * on récupère un pointeur vers l'activité contenante
     *
     * @param context
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof iCallable) {
            mainActivityCallable = (iCallable) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement iCallable ");
        }
    }

    /**
     * on instancie les objets non graphiques
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //
        this.messageListAdapter = new MessageListAdapter(messageList);

        // this.contactList = database.readPerson();
        //this.initData();
    }

    /**
     * on instancie la vue et les composants graphiques
     *
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate ( R.layout.message_list_fragment, container, false );
    }

    @Override
    public void onViewCreated(@Nullable View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated ( view, savedInstanceState );

        this.messageEditText = view.findViewById(R.id.message_edit_text);
        this.messageSendBtn = view.findViewById(R.id.message_send_btn);

        this.messageListRecycleView = (RecyclerView) view.findViewById(R.id.messages_list_recycler_view);

        this.messageListRecycleView.setLayoutManager(new LinearLayoutManager(getActivity()));
        this.messageListRecycleView.setItemAnimator(new DefaultItemAnimator());
        this.messageListRecycleView.setAdapter(this.messageListAdapter);


        // Listeners
        this.messageSendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage(v);
            }
        });
    }

    public void setText(String username) {

        this.fromUser = ContactModel.getCurrentUser();
        this.withUser = new ContactModel(username);

        this.fetchMessage(this.fromUser, this.withUser);
    }

    public void fetchMessage(final ContactModel fromUser, final ContactModel withUser) {
        String url = APIService.URL + "fetchMessages";
        this.getMessageRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Context ctx = getActivity();

                        Toast.makeText(ctx, "Messsage list received", Toast.LENGTH_SHORT).show();

                        addMessages(response, fromUser, withUser);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        Context ctx = getActivity();

                        Toast.makeText(ctx, "MainActivity error :/\n" + error, Toast.LENGTH_SHORT).show();
                        Log.v("MAIN_ACTIVITY", error.toString());
                    }
                }
        ){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                Context ctx                 = getActivity();
                String accessToken          = AuthHelper.readAcessToken(ctx);

                headers.put("Authorization" , "Bearer " + accessToken);

                return headers;
            }
        };

        APIService.getInstance(getActivity()).addToRequestQueue(this.getMessageRequest);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (this.getMessageRequest != null) {
            this.getMessageRequest.cancel();
        }
    }

    private void addMessages(JSONArray messages, ContactModel fromUser, ContactModel withUser) {
        for (int i = 0; i < messages.length(); i++) {
            JSONObject message = null;
            try {
                message = messages.getJSONObject(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            addMessageFromJSONObject(message, fromUser, withUser);
        }

        this.messageListAdapter.notifyDataSetChanged();
    }

    private void addMessageFromJSONObject(JSONObject msg, ContactModel fromUser, ContactModel withUser) {
        try {
            String username = msg.getString("author");
            String messageText = msg.getString("msg");

            if (!(username.equals(fromUser.getUserName()) || username.equals(withUser.getUserName()))) { return; }

            ContactModel author         = username.equals(fromUser.getUserName()) ? fromUser : withUser;
            boolean belongToCurrentUser = fromUser.getUserName().equals(username);
            MessageModel newMessage     = new MessageModel(messageText, author, belongToCurrentUser);

            this.messageList.add(newMessage);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void sendMessage(View view) {
        String newMessage = this.messageEditText.getText().toString();
        if (newMessage.length() > 0) {
            messageList.add(new MessageModel(newMessage, this.fromUser, true));
            this.messageEditText.getText().clear();
        }
    }

    private void initData() {
        ContactModel from = new ContactModel("Issoufi", "Adam", "Issoufi", "test", "The king of MIAGE");
        ContactModel to = new ContactModel("Magutte", "Yassine", "Magutte", "test", "The last day music");

        messageList.add(new MessageModel("Salut", from, true));
        messageList.add(new MessageModel("Salut", to, false));
        messageList.add(new MessageModel("ça va", to, false));
        messageList.add(new MessageModel("Oui et toi", from, true));
    }
}