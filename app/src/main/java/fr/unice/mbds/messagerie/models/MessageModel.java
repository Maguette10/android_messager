package fr.unice.mbds.messagerie.models;

public class MessageModel {
    private String text; // message body
    private ContactModel from; // data of the user that sent this message
    private boolean belongsToCurrentUser; // is this message sent by us?

    public MessageModel(String text, ContactModel from, boolean belongsToCurrentUser) {
        this.text = text;
        this.from = from;
        this.belongsToCurrentUser = belongsToCurrentUser;
    }

    public String getText() {
        return text;
    }

    public ContactModel getEmitter() {
        return from;
    }

    public boolean isBelongsToCurrentUser() {
        return belongsToCurrentUser;
    }
}
