package fr.unice.mbds.messagerie.recycleview.viewholder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import fr.unice.mbds.messagerie.R;

public class ContactListViewHolder extends RecyclerView.ViewHolder {
    public TextView  username,
                     shortdescription;
    public ImageView avatar;

    public ContactListViewHolder(@NonNull View itemView) {
        super(itemView);
        this.username =  itemView.findViewById(R.id.contact_list_recycle_view_username);
        this.shortdescription = itemView.findViewById(R.id.contact_list_recycle_view_short_description);
        this.avatar =  itemView.findViewById(R.id.contact_list_recycle_view_avatar);
    }
}
