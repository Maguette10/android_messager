package fr.unice.mbds.messagerie.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import fr.unice.mbds.messagerie.services.ContactService;

/**
 *
 */
public class ContactHelper extends SQLiteOpenHelper {

    private static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " +
                    ContactService.ContactContract.FeedContact.TABLE_NAME +
                    " (" +
                    ContactService.ContactContract.FeedContact._ID + " INTEGER PRIMARY KEY," +
                    ContactService.ContactContract.FeedContact.COLUMN_NAME_LASTNAME + " TEXT," +
                    ContactService.ContactContract.FeedContact.COLUMN_NAME_FIRSTNAME + " TEXT," +
                    ContactService.ContactContract.FeedContact.COLUMN_NAME_USERNAME + "TEXT," +
                    //ContactService.ContactContract.FeedContact.COLUMN_NAME_PASSWORD + "TEXT," +
                    ContactService.ContactContract.FeedContact.COLUMN_NAME_SHORT_DESCRIPTION + " TEXT" +
                    ")";

    private static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + ContactService.ContactContract.FeedContact.TABLE_NAME;

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "ContactDb.db";
    SQLiteOpenHelper mDbHelper;


    public ContactHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * est appelée par le framework pour accéder à une base de données qui n'est pas encore créée.
     *
     * @param db La représentation Java de la base de données
     */
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    /**
     * est appelée si la version de la base de données est augmentée dans le code de votre
     * application. Cette méthode vous permet de mettre à jour un schéma de base de données
     * existant ou de supprimer la base de données existante et la recréer par la méthode
     * onCreate()
     *
     * @param db         La représentation Java de la base de données
     * @param oldVersion
     * @param newVersion
     */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }
}
