package fr.unice.mbds.messagerie.recycleview.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import fr.unice.mbds.messagerie.R;

public class MessageListViewHolder extends RecyclerView.ViewHolder {
    public View avatar;
    public TextView name;
    public TextView messageBody;

    public MessageListViewHolder(View itemView) {
        super(itemView);
        this.avatar = (View) itemView.findViewById(R.id.avatar);
        this.name = (TextView) itemView.findViewById(R.id.name);
        this.messageBody = (TextView) itemView.findViewById(R.id.message_body);
    }
}