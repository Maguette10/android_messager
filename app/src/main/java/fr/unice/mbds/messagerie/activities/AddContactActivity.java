package fr.unice.mbds.messagerie.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import fr.unice.mbds.messagerie.R;

public class AddContactActivity extends AppCompatActivity {

    private EditText username;
    private Button   confirmBtn;
    private Button   cancelBtn;

    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(	R.layout.add_contact_activity );

        //
        this.username   = findViewById(R.id.add_contact_username_box);
        this.confirmBtn = findViewById(R.id.add_contact_confirm_btn);
        this.cancelBtn  = findViewById(R.id.add_contact_cancel_btn);

        this.cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchToMainActivity();
            }
        });

        this.confirmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchToMainActivity();
            }
        });
    }

    private void switchToMainActivity() {
        Intent intentMainActivity = new Intent(AddContactActivity.this, MainActivity.class);
        startActivity(intentMainActivity);
    }

}
