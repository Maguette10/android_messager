package fr.unice.mbds.messagerie.activities;

import android.content.res.Configuration;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.FrameLayout;

import fr.unice.mbds.messagerie.fragments.ContactListFragment;
import fr.unice.mbds.messagerie.fragments.MessageListFragment;
import fr.unice.mbds.messagerie.R;
import fr.unice.mbds.messagerie.iCallable;

public class MainActivity extends AppCompatActivity implements iCallable {
    // Frame layout
    private FrameLayout contactListFragmentHolder;
    private FrameLayout messageFragmentHolder;

    //
    private Boolean isMainFragment;

    // Fragment
    private ContactListFragment contactListFragment;
    private MessageListFragment messageListFragment;

    //
    private boolean isTheMainFragment = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //
        this.setContentView(R.layout.main_activity);

        //
        this.contactListFragment = new ContactListFragment();
        this.messageListFragment = new MessageListFragment();

        //
        this.contactListFragmentHolder = findViewById(R.id.contactListFragmentHolder);
        this.messageFragmentHolder     = findViewById(R.id.messageFragmentHolder);

        //
        this.switchToContactList();

        // Set the title bar
        this.getSupportActionBar().setTitle("Secure message");
    }

    public void switchToContactList() {
        this.isTheMainFragment = true;

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        transaction.replace(R.id.contactListFragmentHolder, this.contactListFragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    private MessageListFragment switchToMessage() {
        this.isTheMainFragment = false;

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        int currentOrientation = this.getResources().getConfiguration().orientation;

        if (currentOrientation == Configuration.ORIENTATION_LANDSCAPE) {
            transaction.replace(R.id.messageFragmentHolder, this.messageListFragment);
        } else {
            transaction.replace(R.id.contactListFragmentHolder, this.messageListFragment);
        }

        transaction.addToBackStack(null);
        transaction.commit();

        return this.messageListFragment;
    }


    @Override
    public void transferData(String s) {
        this.messageListFragment.setText(s);
        this.switchToMessage();
    }

    public void validate(View v) {
        if (this.isMainFragment) {
            this.switchToMessage();
        }
    }


}
