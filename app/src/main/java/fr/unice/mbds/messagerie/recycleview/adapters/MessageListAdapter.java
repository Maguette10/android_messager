package fr.unice.mbds.messagerie.recycleview.adapters;

import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import fr.unice.mbds.messagerie.R;
import fr.unice.mbds.messagerie.models.MessageModel;
import fr.unice.mbds.messagerie.recycleview.viewholder.MessageListViewHolder;


public class MessageListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<MessageModel> messageList;
    private int IS_BELONGS_TO_CURRENT_USER = 1;

    public MessageListAdapter(List<MessageModel> messageList) {
        this.messageList = messageList;
    }

    public void add(MessageModel message) {
        this.messageList.add(message);
        notifyDataSetChanged(); // to render the list we need to notify
    }

    @Override
    public int getItemViewType(int position) {
        MessageModel messageModel = messageList.get(position);
        return messageModel.isBelongsToCurrentUser() ? IS_BELONGS_TO_CURRENT_USER : -1;
    }

    @Override
    public MessageListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater messageInflater = LayoutInflater.from(parent.getContext());
        View itemView;

        if (viewType == IS_BELONGS_TO_CURRENT_USER) {
            itemView = messageInflater.inflate(R.layout.message_row_emitter, parent, false);
        } else {
            itemView = messageInflater.inflate(R.layout.message_row_receiver, parent, false);
        }

        MessageListViewHolder messageListViewHolder = new MessageListViewHolder(itemView);

        return messageListViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        MessageModel messageModel = messageList.get(position);

        if (messageModel.isBelongsToCurrentUser()) {
            ((MessageListViewHolder) holder).messageBody.setText(messageModel.getText());
        } else {
            ((MessageListViewHolder) holder).messageBody.setText(messageModel.getText());
            ((MessageListViewHolder) holder).name.setText(messageModel.getEmitter().getUserName());
            GradientDrawable drawable = (GradientDrawable) ((MessageListViewHolder) holder).avatar.getBackground();

            drawable.setColor(Color.parseColor(messageModel.getEmitter().getColor()));
        }
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }
}
