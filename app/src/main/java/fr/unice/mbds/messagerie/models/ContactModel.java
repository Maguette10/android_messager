package fr.unice.mbds.messagerie.models;

import java.util.Random;

public class ContactModel {

    private static ContactModel currentUser;

    private String username,
                   firstname,
                   lastname,
                   password,
                   shortdescription,
                   color = "";;

    // Constructor
    public ContactModel(String firstname,
                        String lastname,
                        String username,
                        String password,
                        String shortdescription) {

        this.firstname = firstname;
        this.lastname = lastname;
        this.username = username;
        this.password = password;
        this.shortdescription = shortdescription;
        this.color = this.getRandomColor();

    }

    public ContactModel(String username) {
        this("", "", username, "", "");
        this.setShortdescription(this.getRandomName());
    }

    // Getters
    public String getLastName() {
        return this.lastname;
    }
    public String getFirstName() {
        return this.firstname;
    }
    public String getUserName() {
        return this.username;
    }
    public String getShortdescription() { return this.shortdescription; }
    public String getColor() { return this.color; }

    public static ContactModel getCurrentUser() { return currentUser; }
    public static boolean isLoggedIn() { return currentUser != null; }

    // Setters
    public void setLastName(String lastname) { this.lastname = lastname; }
    public void setFirstName(String firstname) {
        this.firstname = firstname;
    }
    public void setColor(String color) { this.color = color; };
    public void setShortdescription(String newShortDesc) { this.shortdescription = newShortDesc; }

    public static void setCurrentUser(ContactModel user) { currentUser = user; }


    private String getRandomColor() {
        Random r = new Random();
        StringBuffer sb = new StringBuffer("#");
        while(sb.length() < 7){
            sb.append(Integer.toHexString(r.nextInt()));
        }
        return sb.toString().substring(0, 7);
    }

    private String getRandomName() {
        String[] adjs = {"autumn", "hidden", "bitter", "misty", "silent", "empty", "dry", "dark", "summer", "icy", "delicate", "quiet", "white", "cool", "spring", "winter", "patient", "twilight", "dawn", "crimson", "wispy", "weathered", "blue", "billowing", "broken", "cold", "damp", "falling", "frosty", "green", "long", "late", "lingering", "bold", "little", "morning", "muddy", "old", "red", "rough", "still", "small", "sparkling", "throbbing", "shy", "wandering", "withered", "wild", "black", "young", "holy", "solitary", "fragrant", "aged", "snowy", "proud", "floral", "restless", "divine", "polished", "ancient", "purple", "lively", "nameless"};
        String[] nouns = {"waterfall", "river", "breeze", "moon", "rain", "wind", "sea", "morning", "snow", "lake", "sunset", "pine", "shadow", "leaf", "dawn", "glitter", "forest", "hill", "cloud", "meadow", "sun", "glade", "bird", "brook", "butterfly", "bush", "dew", "dust", "field", "fire", "flower", "firefly", "feather", "grass", "haze", "mountain", "night", "pond", "darkness", "snowflake", "silence", "sound", "sky", "shape", "surf", "thunder", "violet", "water", "wildflower", "wave", "water", "resonance", "sun", "wood", "dream", "cherry", "tree", "fog", "frost", "voice", "paper", "frog", "smoke", "star"};
        return (
                adjs[(int) Math.floor(Math.random() * adjs.length)] +
                        "_" +
                        nouns[(int) Math.floor(Math.random() * nouns.length)]
        );
    }
}
