# Messagerie chiffrée de bout en bout

# Description
Conception et réalisation d'une application de messagerie sécurisé sur Android capable de gérer un nombre illimité de conversations simultanées. Les conversations sont chiffrées et sauvegardées dans une base de données.

## Réalisations :
- [x] Conception (diagramme de séquence, maquettes des écrans, etc.)
- [x] Réalisation des écrans (Login, Register, ContactList, etc.)
- [x] Réalisation des traitements dans les écrans et modèle de donnée 
- [x] Base de données et service réseau

## Environnement Technique :
- Langage : Java
- OS :  macOS
- Framwork : Java Development Kit, Android SDK
- Outil : Android Studio, Postman, Git, SourceTree, GitLab, Trello, Draw.io

# Identifiant de démo

username: `admin`

password: `password`

